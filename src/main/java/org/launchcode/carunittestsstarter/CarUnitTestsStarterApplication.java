package org.launchcode.carunittestsstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarUnitTestsStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarUnitTestsStarterApplication.class, args);
	}

}
